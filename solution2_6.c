#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<unistd.h>
#include<time.h>
#include<string.h>

#define SIZE 2048
#define Ram (SIZE-1)

int tmp[SIZE];

void swap(int sw[SIZE]){
  int tm,ran1,ran2;
  ran1=(int)(rand()%Ram+1);
  ran2=(int)(rand()%Ram+1);
  //printf("ran1=%d,ran2=%d\n",ran1,ran2);
  tm=tmp[ran1];
  tmp[ran1]=tmp[ran2];
  tmp[ran2]=tm;
}
int main(){
  FILE *fp1,*fp2;
  char *fname1="input_6.csv",*fname2="solution_yours_6.csv";
  double x[SIZE],y[SIZE];
  char a,b;
  double kyori=0,kyori1=0,kyori2=0,kyori3=0;
  int i=0,j,k,l,n,m,ret,memo1,memo2=0,count,first,last;
  int zahyou1=0,zahyou2=0;
  int ans[SIZE];

  //ファイル読み込み
  fp1=fopen(fname1,"r");
  if( fp1 == NULL ){
    printf( "ファイルが開けません\n" );
    return -1;
  }
  fscanf(fp1,"%c,%c",&a,&b);

  while((ret=fscanf(fp1,"%lf,%lf",&x[i],&y[i]))!=EOF){
    // printf( "%lf %lf\n",x[i],y[i]);
      i++;
  }
 
  fclose(fp1);



  //経路探索
  kyori1=(sqrt((x[1]-x[0])*(x[1]-x[0])+(y[1]-y[0])*(y[1]-y[0])));
  kyori2=kyori1;
  tmp[0]=0;
  for(k=0;k<SIZE-1;k++){
    // printf("********k=%d*********\n",k);
    tmp[k+1]=k;
    count=0;
    memo2=0;
    for(j=0;j<SIZE;j++){
      memo1=0;
      for(l=0;l<k+1;l++){
	//printf("tmp[%d]=%d,j=%d\n",l,tmp[l],j);
	if(tmp[l]==j){
	  memo1=1;
	  break;
	}
      }//l
      /* if(j==0){
	kyori2=kyori1;
	}*/
      // printf("memo1=%d,memo2=%d\n",memo1,memo2);
      if(memo1!=1 && j!=k){
	count++;
	kyori1=(sqrt((x[j]-x[k])*(x[j]-x[k])+(y[j]-y[k])*(y[j]-y[k])));
	//printf("kyori1=%lf,kyori2=%lf\n",kyori1,kyori2);
	if(count==1){
	  kyori2=kyori1;
	  tmp[k+1]=j;
	  memo2=j;
	}
	if(kyori1<kyori2){
	  kyori2=kyori1;
	  tmp[k+1]=j;
	  memo2=j;
	  // printf("tmp[%d]=%d,memo2=%d,kyori2=%lf\n",k+1,j,memo2,kyori2);
	}
      }
    }//j
    kyori+=kyori2;
    //printf("%d~%d\nkyori=%lf+%lf=%lf\n",k,memo2,kyori-kyori2,kyori2,kyori);
  }//k
  last=tmp[SIZE-1];
  kyori+=(sqrt((x[0]-x[last])*(x[0]-x[last])+(y[0]-y[last])*(y[0]-y[last])));

  srand((unsigned)time(NULL));
  int sw;
  sw=SIZE;
  for(n=0;n<sw;n++){
    kyori3=0;
    swap(tmp);
    for(l=0;l<SIZE-1;l++){
      zahyou1=tmp[l];
      zahyou2=tmp[l+1];
      kyori3+=(sqrt((x[zahyou2]-x[zahyou1])*(x[zahyou2]-x[zahyou1])+(y[zahyou2]-y[zahyou1])*(y[zahyou2]-y[zahyou1])));
    }
    last=tmp[SIZE-1];
    first=tmp[0];
    kyori3+=(sqrt((x[first]-x[last])*(x[first]-x[last])+(y[first]-y[last])*(y[first]-y[last])));
    if(kyori3<kyori){
      kyori=kyori3;
      printf("kyori=%lf\n",kyori);
      for(m=0;m<SIZE;m++){
	ans[m]=tmp[m];
      }
    }
  }
  
  printf("answer=%lf\n",kyori);

  //ファイル書き込み
  fp2=fopen(fname2,"w");
  if( fp2 == NULL ){
    printf( "ファイルが開けません\n" );
    return -1;
  }

  fprintf(fp2,"index\n");
  for(i=0;i<SIZE;i++){
    fprintf(fp2,"%d\n",ans[i]);
  }
  fclose(fp2);
}
  
