#include<stdio.h>
#include<stdlib.h>
#include<math.h>

#define SIZE 128

int main(){
  FILE *fp;
  char *fname="input_4.csv";
  double x[SIZE],y[SIZE];
  double kyori=0;
  int i=0,j,ret;
  char a,b;

  //ファイル読み込み
  fp=fopen(fname,"r");
  if( fp == NULL ){
    printf( "ファイルが開けません\n" );
    return -1;
  }
  fscanf(fp,"%c,%c",&a,&b);

  while((ret=fscanf(fp,"%lf,%lf",&x[i],&y[i]))!=EOF){
    //printf( "%lf %lf\n",x[i],y[i]);
      i++;
  }
  printf("\n");
  fclose(fp);

  //経路探索
  for(j=0;j<SIZE-1;j++){
    kyori+=(sqrt((x[j+1]-x[j])*(x[j+1]-x[j])+(y[j+1]-y[j])*(y[j+1]-y[j])));
  }
  kyori+=(sqrt((x[0]-x[SIZE-1])*(x[0]-x[SIZE-1])+(y[0]-y[SIZE-1])*(y[0]-y[SIZE-1])));

  printf("answer=%lf\n",kyori);

}
  
