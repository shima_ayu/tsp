#include<stdio.h>
#include<stdlib.h>
#include<math.h>

#define SIZE 5

int main(){
  FILE *fp1,*fp2;
  char *fname1="input_0.csv",*fname2="file.csv";
  double x[SIZE],y[SIZE];
  double kyori=0;
  int i=0,j,ret;
  char a,b;

  //ファイル読み込み
  fp1=fopen(fname1,"r");
  if( fp1 == NULL ){
    printf( "ファイルが開けません\n" );
    return -1;
  }
  fscanf(fp1,"%c,%c",&a,&b);

  while((ret=fscanf(fp1,"%lf,%lf",&x[i],&y[i]))!=EOF){
    // printf( "%lf %lf\n",x[i],y[i]);
    i++;
  }
 
  fclose(fp1);

  //ファイル書き込み
  fp2=fopen(fname2,"w");
  if( fp2 == NULL ){
    printf( "ファイルが開けません\n" );
    return -1;
  }

  for(j=0;j<SIZE;j++){
    fprintf(fp2,"%lf %lf\n",x[j],y[j]);
  }

  fclose(fp2);

}
